// import React from 'react';
import React, { useContext } from 'react';
import { Route, BrowserRouter as Router, Routes } from 'react-router-dom';
import { AuthContext } from "../src/context/authContext";
import PrivateRoutes from './Utils/PrivateRoutes';
import SideBar from './components/SideBar/SideBar.jsx';
import AddPost from './components/WritePost/Add';
import TestAppNavbar from './components/navbar/NavBarTest.jsx';
import Home from './pages/Home';
import Errorpage from './pages/ErrorPage/ErrorPage';
import GotoTop from './components/GtoTop/ButtonTop';
import LogIn from './pages/LogIn/LogIn';
// Subpages
import OverView from './pages/SubPages/OverView/Overview';

// History
import HistoryBarangay from './pages/SubPages/History-Town/Barangay/HistoryBarangay';
import HistoryTown from './pages/SubPages/History-Town/HistoryTown';
// Transparency
import InvitationToBid from './pages/SubPages/Transparency/BidsAndAwards/InvitationToBid/InvitationToBid';
import NoticeOfAwards from './pages/SubPages/Transparency/BidsAndAwards/NoticeOfAwards/NoticeOfAwards';
import NoticeOfNegotiatedProcurement from './pages/SubPages/Transparency/BidsAndAwards/NoticeOfNegotiatedProcurement/NoticeOfNegotiatedProcurement';
import NoticeToProceed from './pages/SubPages/Transparency/BidsAndAwards/NoticeToProceed/NoticeToProceed';
import RequestForQuotation from './pages/SubPages/Transparency/BidsAndAwards/RequestForQuotation/RequestForQuotation';
// css
import "./App.css";

import 'bootstrap/dist/css/bootstrap.min.css';
function App() {

  const { isAuthenticated } = useContext(AuthContext);

  return (
    <Router>

      {/* MAIN */}

      {!isAuthenticated ? <TestAppNavbar />
        : null}
      <Routes>

        {/* private routes if logged in only access */}
        <Route element={<PrivateRoutes />}>
          <Route path="/add" element={<AddPost />} />
          <Route path="/sidebar" element={<SideBar />} />
        </Route>
        {/* private routes if logged in only access */}

        {/* Public Routes */}
        <Route path="/" element={<Home />} />
        {/* subpages */}
        {/* overview */}
        <Route path="/Overview" element={<OverView />} />
        {/* history */}
        <Route path="/History/HistoryTown" element={<HistoryTown />} />
        <Route path="/History/HistoryBarangay" element={<HistoryBarangay />} />
        {/* transparency */}
        <Route path="/Transparency/InvitationToBid" element={<InvitationToBid />} />
        <Route path="/Transparency/NoticeOfAwards" element={<NoticeOfAwards />} />
        <Route path="/Transparency/NoticeOfNegotiatedProcurement" element={<NoticeOfNegotiatedProcurement />} />
        <Route path="/Transparency/NoticeToProceed" element={<NoticeToProceed />} />
        <Route path="/Transparency/RequestForQuotation" element={<RequestForQuotation />} />
        {/* <Route path="/Overview" element={<OverView />} /> */}

        <Route path="/login" element={<LogIn />} />
        <Route path="*" element={<Errorpage isAuthenticated={isAuthenticated} />} />
        {/* Public Routes */}
      </Routes>

      <GotoTop />
    </Router>

  );
}

export default App;

