import React,{useState} from 'react'
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import './WritePostTest.scss'

function WritePostTest() {
    const [value, setValue] = useState("");

return (
    <div className="add">
        <div className="content--write-post">
            <input
                type="text"
                placeholder="Title"
                // onChange={(e) => setTitle(e.target.value)}
            />
            <div className="editorContainer">
                <ReactQuill
                    className="editor"
                    theme="snow"
                    value={value}
                    onChange={setValue}
                />
            </div>
        </div>
        <div className="menu--write--post">
            <div className="item">
                <h1>Publish</h1>
                <span>
                    <b>Status: </b> Draft
                </span>
                <span>
                    <b>Visibility: </b> Public
                </span>
                {/* upload  file*/}
                {/* <input
                    style={{ display: "none" }}
                    type="file"
                    id="file"
                    name=""
                    // onChange={(e) => setFile(e.target.files[0])}
                />
                <label className="file" htmlFor="file">
                    Upload Image
                </label> */}
                 {/* upload  file*/}
                <div className="buttons--menu">
                    <button >Save as a draft</button>
                    <button >Publish</button>
                </div>
            </div>
            <div className="item">
                <h1>Category</h1>
                <div className="cat">
                    <input
                        type="radio"
                        // checked={cat === "art"}
                        checked="PESO NEWS"
                        name="cat"
                        value="PESO NEWS"
                        id="PESO NEWS"
                        // onChange={(e) => setCat(e.target.value)}
                    />
                    <label htmlFor="PESO NEWS">PESO NEWS</label>
                </div>
                <div className="cat">
                    <input
                        type="radio"
                        // checked={cat === "science"}
                        checked="MUNICIPAL NEWS"
                        name="cat"
                        value="MUNICIPAL NEWS"
                        id="MUNICIPAL NEWS"
                        // onChange={(e) => setCat(e.target.value)}
                    />
                    <label htmlFor="MUNICIPAL NEWS">MUNICIPAL NEWS</label>
                </div>
                <div className="cat">
                    <input
                        type="radio"
                        // checked={cat === "technology"}
                        checked="LATEST NEWS"
                        name="cat"
                        value="LATEST NEWS"
                        id="LATEST NEWS"
                        // onChange={(e) => setCat(e.target.value)}
                    />
                    <label htmlFor="LATEST NEWS">LATEST NEWS</label>
                </div>
                {/* <div className="cat">
                    <input
                        type="radio"
                        // checked={cat === "cinema"}
                        checked="cinema"
                        name="cat"
                        value="cinema"
                        id="cinema"
                        // onChange={(e) => setCat(e.target.value)}
                    />
                    <label htmlFor="cinema">Cinema</label>
                </div>
                <div className="cat">
                    <input
                        type="radio"
                        // checked={cat === "design"}
                        checked="design"
                        name="cat"
                        value="design"
                        id="design"
                        // onChange={(e) => setCat(e.target.value)} 
                    />
                    <label htmlFor="design">Design</label>
                </div>
                <div className="cat">
                    <input
                        type="radio"
                        // checked={cat === "food"}
                        checked="food"
                        name="cat"
                        value="food"
                        id="food"
                        // onChange={(e) => setCat(e.target.value)}
                    />
                    <label htmlFor="food">Food</label>
                </div> */}
            </div>
        </div>
    </div>
)
}

export default WritePostTest