import React, { useEffect, useState, useRef } from "react";
import "./MO.scss";
function CounterMO(props) {
// !!function for count
// const counter = (minimum, maximum) => {
//   for (let count = minimum; count <= maximum; count++) {
//     setTimeout(() => {
//       setCount(count);
//     }, 1000 * count);
//   }
// };

// !!useEffect for counter
// useEffect(() => {
//   counter(0, 75);
// }, []);
//
//
    const [count, setCount] = useState(0);
    const prevCountRef = useRef();
    
    useEffect(() => {
        console.log('CHECK')
      const counter = (minimum, maximum) => {
        for (let count = minimum; count <= maximum; count++) {
            
          setTimeout(() => {
            if (prevCountRef.current !== count) {
              //Number of count eg: count = 1 * 3 = 3 3*3 // 3 because of useRef and so on
              setCount(count*3);
            }
            // mili second before the counter starts
          }, 1500);
        }
      };
  
      counter(0, props.value);
      prevCountRef.current = count;
    }, [props.value]);
    
  return (
    // <div >{count < props.value ? count : props.value}</div>
    <div className="card--MOtitle">{count < props.value ? count : props.value}</div>
  )
}

export default CounterMO