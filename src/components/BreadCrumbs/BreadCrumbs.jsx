import React from 'react'
import Breadcrumb from 'react-bootstrap/Breadcrumb';


function BreadCrumbs(props) {
  return (


    <Breadcrumb>
      {props.items.map((item, index) => (
        <Breadcrumb.Item key={index} href={item.url}>
          {item.label}
        </Breadcrumb.Item>
      ))}
    </Breadcrumb>

    // <Breadcrumb>
    //   <Breadcrumb.Item href="#">Home</Breadcrumb.Item>
    //   <Breadcrumb.Item href="https://getbootstrap.com/docs/4.0/components/breadcrumb/">
    //     Library
    //   </Breadcrumb.Item>
    //   <Breadcrumb.Item active>Data</Breadcrumb.Item>
    // </Breadcrumb>


  )
}

export default BreadCrumbs