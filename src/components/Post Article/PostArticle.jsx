import React, { useEffect, useState } from 'react';
import { Card, Form, Col, Container, Row, FormControl, Button } from 'react-bootstrap';
import { Link, } from 'react-router-dom';
import "./SingleArticleView.css";
import articleData from "./articles.json";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import { solid, regular, brands, icon } from '@fortawesome/free-solid-svg-icons'


function SingleArticleView() {

    const [] = useState()

    return (

        <Container fluid>
            <div className="singleArticleContainer">
                <Row xs={1} md={2} className="g-4">
                    {/* 1st column */}
                    <Col className='articleView' md={8}>
                        <Card>
                            <div className="single">
                                <div className="content">
                                    <img src='https://www.thoughtco.com/thmb/cAvGc4QBGDdGWTuxkbkhOyx2UHc=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc():format(webp)/166370483-56cb36da5f9b5879cc54103c-5c54ad6b46e0fb00013a2205.jpg' alt="try-img" />

                                    <div className="user">
                                        <img
                                            id='cardImgAuthor'
                                            src='https://scontent.fmnl13-1.fna.fbcdn.net/v/t1.6435-9/83185366_907843982946130_2332233001972269056_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeEf-ksbWpIAit2p-gtUEujQhsUtNGZER4KGxS00ZkRHgvMnTaVBUDVDj82c6q2A_HUavq1UFs5Ro27LpWpTmKUb&_nc_ohc=fR808a1-4EcAX9Xqaw7&_nc_ht=scontent.fmnl13-1.fna&oh=00_AfAFH7EPh5RtLJHv7nhanPEFtQT-dlYGxEdnqqiVCmQ27g&oe=64140C86'
                                            alt="user-img"
                                        />
                                        <div className="info">
                                            <span>Lorem Ipsum</span>
                                            <small>Date Posted Here</small>
                                        </div>
                                        <div className="edit">
                                            <Link to={`/`} state=''>
                                                <img src='' alt="" />
                                            </Link>
                                            <img onClick='' src='{Delete}' alt="" />
                                        </div>
                                    </div>
                                    <h1>Title Here</h1>
                                    <p
                                    // dangerouslySetInnerHTML={{
                                    //     __html: DOMPurify.sanitize(post.desc),
                                    // }}
                                    >Incididunt voluptate ut sit eu nostrud incididunt incididunt veniam ex. Aliqua elit mollit in incididunt Lorem qui ex. Tempor est aliquip nisi est exercitation nostrud adipisicing cillum aliqua et et in. Cillum ex sunt pariatur ipsum cupidatat incididunt laboris amet irure non Lorem. Cupidatat pariatur tempor mollit nisi ut voluptate in dolore amet nulla reprehenderit. Et mollit adipisicing ut velit labore sint do nostrud quis officia.</p>
                                </div>
                                {/* <Menu cat={post.cat} /> */}
                            </div>

                        </Card>
                    </Col>

                    {/* 2nd column */}
                    <Col className='category' md={4}>
                        <Form className="d-flex searchForm">
                            <FormControl
                                type='search'
                                placeholder='Search'
                                className='me-2'
                                aria-label='Search'
                            />
                            <Button variant='outline-success'>
                                <i className="fa-solid fa-magnifying-glass"></i>
                            </Button>
                        </Form>
                        <Card>
                            <div className='containerCategory'>
                                <h2 className='Categories'>Category</h2>
                                {/* <p>Hello, <FontAwesomeIcon icon={faUser} />!</p> */}
                                <ul>
                                    <li className='categoryList'><i className="fa-regular fa-star"></i>Bulan Municipio News</li>
                                    <li className='categoryList'><i className="fa-regular fa-star"></i>Latest News</li>
                                    <li className='categoryList'><i className="fa-regular fa-star"></i>PESO Updates</li>
                                </ul>
                            </div>
                        </Card>
                    </Col>
                </Row>
            </div>
        </Container>
    )
}

export default SingleArticleView