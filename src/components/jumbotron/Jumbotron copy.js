import React, { useState, useEffect } from 'react'
import "./jumbotron.scss"
import { Carousel, Button } from "react-bootstrap";
import coastal from "../../Img/Bulan-Tourism/CoastalWater-cln.jpg"
import park from "../../Img/Bulan-Tourism/rizal park2.jpg"
import padaraw from "../../Img/Bulan-Tourism/Padaraw1.jpg"

function Jumbotron() {

  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    console.log(`check Loaded txt animation ${loaded}`)
    setLoaded(true);
  }, []);

  return (
    <div>
      <Carousel fade>
      {/* <Carousel.Item>
        <img
          className="d-block w-100"
          src={coastal}
          alt="First slide"u
        />
        <Carousel.Caption>
          <h3>First slide label</h3>
          <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
        </Carousel.Caption>
      </Carousel.Item> */}

      <Carousel.Item>
        <div className='div-carousel-img1  '>
          <img
            className="d-block w-100 carousel-img1"
            src={coastal}
            alt="slide1"
          />
        <Carousel.Caption>
           <div className='carousel-caption1'>
              <div className='div-carousel-desc'>
                
                <span className={`crsl-spn-1 ${loaded ? "loaded" : ""}`}><b>Endowed </b>with </span>
                <span className={`crsl-spn-2 ${loaded ? "loaded" : ""}`}><b>Natural</b> resources</span>
                <p className={`crsl-p ${loaded ? "loaded2" : ""}`}>and wonderful sceneries.</p>
                <button className={`crs1-btn ${loaded ? "loaded2" : ""}`}>Explore!</button>
              </div >
           </div> 
        </Carousel.Caption>
        </div >
        
      </Carousel.Item>

      {/* <Carousel.Item>
        <img
          className="d-block "
          src={park}
          alt="Second slide"
        />

        <Carousel.Caption>
          <h3>Second slide label</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </Carousel.Caption>
      </Carousel.Item> */}
 {/*
      <Carousel.Item>
        <img
          className="d-block "
          src={padaraw}
          alt="Third slide"
        />

        <Carousel.Caption>
          <h3>Third slide label</h3>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          </p>
        </Carousel.Caption>
      </Carousel.Item>

      <Carousel.Item>
        <img
          className="d-block "
          src={padaraw}
          alt="Third slide"
        />

        <Carousel.Caption>
          <h3>4th slide label</h3>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          </p>
        </Carousel.Caption>
      </Carousel.Item> */}
    </Carousel>
    </div>
    
  )
}

export default Jumbotron

