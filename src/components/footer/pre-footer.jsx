import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import "./pre-footer.css";

function prefooter() {
  return (
    <div className="div--Prefooter">
      <Row xs={1} md={3}>
        <Col >
          <div className="div-prefooter-logo">
            <img
              className="img-fluid img__prefooterlogo p-3"
              src="/transparency-seal-resized.png"
              alt="Transparency Logo"
            />
          </div >
        </Col>

        <Col xs={12}>
          <div className="div__PrefooterContact">
            <h4>Contact Info</h4>
            <br></br>

            <div className="d-flex  mb-3">
              <Row
                xs={1}
                md={2}
                className="justify-content-center align-items-center"
              >
                <Col xs={2} md={2}>
                  <i className="fa-solid fa-building-columns icons__prefooter"></i>
                </Col>
                <Col xs={10} md={10}>
                  <h5 className="my-0 px-3 ftr-itm-title">Visit The Municipal Hall</h5>
                  <p className="my-0 px-3 ">Brgy. Aquino BUlan, Sorsogon</p>
                </Col>
              </Row>
            </div>
            
            <div className="d-flex  mb-3">
              <Row
                xs={1}
                md={2}
                className="justify-content-center align-items-center"
              >
                <Col xs={2} md={2}>
                  <i className="fa-solid fa-envelope-open-text icons__prefooter"></i>
                </Col>
                <Col xs={10} md={10}>
                  <h5 className="my-0 px-3 ftr-itm-title">Email</h5>
                  <p className="my-0 px-3 ftr-itm-dsc">lgubulan@gmail.com</p>
                </Col>
              </Row>
            </div>


            <div className="d-flex mb-3">
              <Row
                xs={1}
                md={2}
                className="justify-content-center align-items-center"
              >
                <Col xs={3} sm={2} md={2}>
                  <i className="fa-solid fa-phone-volume icons__prefooter"></i>
                </Col>
                <Col xs={9}  md={10} className="">
                  <h5 className="my-0 px-3 ftr-itm-title">Phone</h5>
                  <p className="my-0 px-3 ftr-itm-dsc">(123)456-7890</p>
                </Col>
              </Row>
            </div>
            

            
          </div>
        </Col>

        <Col xs={12}>
          <div className="div__PreFooterPhoneNumbers">
            <h4>Phone Numbers</h4>
            <br></br>
            <h6>MDRRMO/Rescue</h6>
            <p>09618234132</p>
            <h6>PNP</h6>
            <p>09198292101</p>
            <h6>BFP</h6>
            <p>09301382814</p>
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default prefooter;
