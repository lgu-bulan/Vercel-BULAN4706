import React from "react";
import { Col, Row } from "react-bootstrap";
import "./DateTime.scss";

function DateTime() {
  // const [dateTime, setDateTime] = useState(new Date());

  // useEffect(() => {
  //   const intervalId = setInterval(() => {
  //     setDateTime(new Date());
  //   }, 1000);

  //   return () => clearInterval(intervalId);
  // }, []);

  // const day = dateTime.toLocaleDateString(undefined, { weekday: "long" });
  // const time = dateTime.toLocaleTimeString();

  return (
    <div className="dateTimeBody">
      <div className="container-dateTimeBody">
        <Row xs={1} sm={1} md={2} className="banner-row">
          <Col xs={12} md={6} className="col-lgu-bulan-logo">
            <div className="wrapper-lgu-bulan  mx-3">
              <Row xs={2} md={2} className="banner_inner_row">
                <Col xs={4} md={4} className="col_lgu_bulan_logo">
                  <img
                    className="lgu_bulan_logo mx-1"
                    src="https://bulan4706.com/wp-content/uploads/2022/10/lgo.png"

                    alt="LGU BULAN LOGO"
                  />
                </Col>
                <Col xs={8} md={8} className="col_lgu_bulan_name">
                  <h1>Bulan 4706</h1>
                  <div className="underline-banner"></div>
                  <h3>Home of the Padaraw Festival!</h3>
                </Col>
              </Row>
            </div>
          </Col>

        </Row>
      </div>
    </div>
  );
}

export default DateTime;
