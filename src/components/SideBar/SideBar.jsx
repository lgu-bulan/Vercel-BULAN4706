import React, { useContext, useEffect, useRef, useState } from "react";
import { Link, Route, Routes, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import profpic from "../../Img/profile-picture.png";
import { AuthContext } from "../../context/authContext";
import WritePostTest from "../WritePost/WritePostTest";
import Dashboard from "./DashBoard";
import "./SideBar.scss";
// SIDEBAR
function SideBar() {
  const {
    unsetUser,
    currentUser,
    setCurrentUser,
    setIsAuthenticated,
    isAuthenticated,
  } = useContext(AuthContext);
  const [mode, setMode] = useState(
    localStorage.getItem("mode") === "dark" ? "dark" : "light"
  );

  const navigate = useNavigate();
  const bodyRef = useRef(null);

  useEffect(() => {
    bodyRef.current.classList.toggle("dark", mode === "dark");
    localStorage.setItem("mode", mode);
  }, [mode]);

  // SWITCH
  const handleModeToggle = () => {
    setMode(mode === "light" ? "dark" : "light");
  };

  //   dashboard
  const [status, setStatus] = useState(
    () => localStorage.getItem("status") || "open"
  );

  const sidebarRef = useRef(null);

  useEffect(() => {
    sidebarRef.current.classList.toggle("close", status === "close");
    console.log(status);
    localStorage.setItem("status", status);
  }, [status]);

  const handleSidebarToggle = () => {
    setStatus(status === "open" ? "close" : "open");
  };

  const handleLogout = async () => {
    // Display the swal to confirm that the user wants to log out
    const { isConfirmed } = await Swal.fire({
      title: "Are you sure?",
      text: "You will be logged out.",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, log out",
    });

    // If the user confirms the swal, log out and remove the access token
    if (isConfirmed) {
      try {
        // localStorage.clear(); // Clear all items from local storage
        unsetUser()
        await Swal.fire("Logged Out!", "success");
        setIsAuthenticated(false);
        navigate("/");
      } catch (error) {
        console.error(error);
      }
    }
  };
  const menuItem = [
    {
      path: "/",
      name: "Dashboard",
      icon: <i className="fa-solid fa-house sidebar-icon"></i>,
    },
    {
      path: "/add",
      name: "Content",
      icon: <i className="fa-solid fa-pen-to-square sidebar-icon"></i>,
    },
    {
      path: "/analytics",
      name: "Asset",
      icon: <i className="fa-sfolid fa-link sidebar-icon"></i>,
    },
  ];

  return (
    <div id="body-div" ref={bodyRef}>
      <div className="my-admin-dashboard">
        <nav className="Sidebar--Nav" ref={sidebarRef}>
          <div className="div_Sidebar_Nav_Brand">
            <div className="div_Sidebar_image">
              <img
                src="https://bulan4706.com/wp-content/uploads/2022/10/lgo.png"
                alt="bulan4706Logo"
              />
            </div>

            <span className="span_Sidebar_logo_name">Bulan-4706</span>
          </div>

          <div className="Sidebar--menu-items">
            <ul className="Sidebar--nav-links">
              <li>
                <Link to="/">
                  <i className="fa-solid fa-house sidebar-icon"></i>
                  <span className="link-name">Dahsboard</span>
                </Link>
              </li>
              <li>
                <Link to="/add2">
                  <i className="fa-solid fa-pen-to-square sidebar-icon"></i>
                  <span className="link-name">Content</span>
                </Link>
              </li>
              <li>
                <a href="/">
                  {/* <i className="fa-solid fa-images"></i> */}
                  <i className="fa-solid fa-link sidebar-icon"></i>
                  <span className="link-name">Asset</span>
                </a>
              </li>

              <li>
                <a href="/">
                  <i className="fa-solid fa-share sidebar-icon"></i>
                  <span className="link-name">Share</span>
                </a>
              </li>
            </ul>

            <ul className="logout-mode">
              <li>
                <a href="#" onClick={handleLogout}>
                  <i className="fa-solid fa-right-from-bracket sidebar-icon"></i>
                  <span className="link-name">Logout</span>
                </a>
              </li>

              <li className="mode">
                <a href="/">
                  {mode === "dark" ? (
                    <>
                      <i className="fas fa-moon sidebar-icon"></i>
                      <span className="link-name">Dark Mode</span>
                    </>
                  ) : (
                    <>
                      <i className="fas fa-sun"></i>
                      <span className="link-name">Light Mode</span>
                    </>
                  )}

                  {/* <i className="fa-solid fa-moon sidebar-icon"></i>
                  <span className="link-name">Dark Mode</span> */}
                  {/* <span className="link-name">Dark Mode</span> */}
                </a>

                <div className="mode-toggle">
                  <span
                    className="switch"
                    checked={mode === "dark"}
                    onClick={handleModeToggle}
                  ></span>
                  {/* <Switch  className="switch-mode" checked={mode === "dark"} onChange={handleModeToggle}></Switch> */}
                </div>
              </li>
            </ul>
          </div>
        </nav>
        {/* !!DASHBOARD */}

        <div className="dashboard">
          {/* TOP HEADER */}
          <div className="top">
            <i
              className="fas fa-bars sidebar-toggle"
              // className="fa-solid fa-bars-staggered sidebar-toggle"
              onClick={handleSidebarToggle}
            ></i>

            <div className="search-box">
              <i className="fa-solid fa-magnifying-glass"></i>
              <input type="text" placeholder="Search here..." />
            </div>
            <div className="current-user-dashboard-top">
              <i className="fa-solid fa-bell top-bell"></i>
              <span className="span-currentuser">MARK ELVIN SIMORA</span>
              <img src={profpic} alt="crruserImg" />
            </div>
          </div>
          {/* END TOP */}
          <div className="side-content-area">
            <div
              className="BreadCrumbs"
              style={{ border: "red solid 2px" }}
            ></div>
            <Routes>
              {/* {isAuthenticated ? null : null}
              {!isAuthenticated ? null : null} */}
              {/* {isAuthenticated ? null : null}
              {!isAuthenticated ? null : null} */}
              <Route
                path="/DashBoard"
                element={
                  <Dashboard
                    status={status}
                    handleSidebarToggle={handleSidebarToggle}
                  />
                }
              />
              <Route path="/add2" element={<WritePostTest />} />
            </Routes>
          </div>
          {/* END TOP HEADER */}

          {/* <Dashboard
            status={status}
            handleSidebarToggle={handleSidebarToggle}
          /> */}
        </div>
      </div>
    </div>
  );
}

export default SideBar;
