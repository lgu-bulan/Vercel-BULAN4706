import { Table, Pagination, Input, Tag } from "antd";
import qs from "qs";
import axios from "axios";
import "./Table.scss";
import { useEffect, useState } from "react";

function TestTABLE ()  {
  const [data, setData] = useState();
  const [searchedText, setSearchedText] = useState("");
  const [loading, setLoading] = useState(false);
  const [totalItems, setTotalItems] = useState();
  const [tableParams, setTableParams] = useState({
    pagination: {
      current: 1,
      pageSize: 10,
      position: ["bottomCenter"],
      showQuickJumper: true,
      showSizeChanger: true,
    },
  });

  const columns = [
    {
      title: "Status",
      key: "status",
      dataIndex: "status",
      width: "100px",
      align: "center",
      fixed: "left",

      render: (_, { status }) => {
        // let color = status.length > 5 ? 'geekblue' : 'green';
        let color = "";
        if (status === "Draft") {
          color = "volcano";
        }
        if (status === "Published") {
          color = "geekblue";
        }
        return (
          <Tag color={color} key={status}>
            {status.toUpperCase()}
          </Tag>
        );
      },
    },
    {
      title: "PostID",
      dataIndex: "id",
      width: "80px",
      align: "center",
    },
    {
      title: "User",
      dataIndex: "uid",
      width: "80px",
      align: "center",
    },
    {
      title: "Title",
      dataIndex: "title",
      filteredValue: [searchedText],
      onFilter: (value, record) => {
        return (
          String(record.title).toLowerCase().includes(value.toLowerCase()) ||
          String(record.uid).toLowerCase().includes(value.toLowerCase()) ||
          String(record.id).toLowerCase().includes(value.toLowerCase())
        );
        // return String(record.title.toLowerCase().includes(value.toLowerCase()));
      },

      width: "130px",
    },
    {
      title: "Description",
      dataIndex: "description",
      width: "160px",
      ellipsis: true,
    },
    {
      title: "Date",
      dataIndex: "date",
      width: "110px",
    },
    {
      title: "Category",
      key: "category",
      dataIndex: "category",
      width: "160px",

      render: (_, { category }) => {
        // let color = status.length > 5 ? 'geekblue' : 'green';
        let color = "";
        if (category === "LATEST NEWS") {
          color = "#5cdbd3";
        }
        if (category === "PESO UPDATES") {
          color = "#ff85c0";
        }
        if (category === "MUNICIPAL NEWS") {
          color = "#b37feb";
        }
        return (
          <Tag color={color} key={category}>
            {category.toUpperCase()}
          </Tag>
        );
      },
    },
    {
      title: "Action",
      dataIndex: "action here",
      width: "130px",
      fixed: "right",
    },
  ];

  const fetchData = (page) => {
    setLoading(true);
    const { current, pageSize } = tableParams.pagination;
    axios
      .get(`http://localhost:5000/api/all?page=${current}&pageSize=${pageSize}`)
      .then((res) => {
        const modifiedData = res.data.map((item) => ({
          ...item,
          key: item.id,
        }));
        console.log("res DAta", res.data.data);
        console.log("res DAta length", res.data.length);
        console.log("trying to get id ", modifiedData);
        console.log("total ", modifiedData.length);
        // console.log()
        setTotalItems(modifiedData.length);
        setData(modifiedData);
        setLoading(false);
        setTableParams({
          ...tableParams,
          pagination: {
            ...tableParams.pagination,
            total: modifiedData.length,
            // total: res.data.totalCount,
          },
        });
      });
  };

  useEffect(() => {
    fetchData();
  }, [JSON.stringify(tableParams)]);

  const handleTableChange = (pagination, filters, sorter) => {
    setTableParams({
      pagination,
      filters,
      ...sorter,
    });

    if (pagination.pageSize !== tableParams.pagination?.pageSize) {
      // reset data to an empty array
      setData([]);

      // update the pagination object with the new pageSize
      setTableParams({
        ...tableParams,
        pagination: {
          ...tableParams.pagination,
          pageSize: pagination.pageSize,
          current: 1, // reset current page to 1
        },
      });
    }
  };

  const handlePaginationChange = (page, pageSize) => {
    setTableParams({
      ...tableParams,
      pagination: {
        ...tableParams.pagination,
        current: page,
        pageSize,
      },
    });
  };

  const handlePageSizeChange = (current, size) => {
    setTableParams({
      ...tableParams,
      pagination: {
        ...tableParams.pagination,
        current: 1,
        pageSize: size,
      },
    });
  };
  // const showTotalData = tableParams.pagination.map((total) => { return console.log(total) });

  return (
    <div className="div-postTable">
      {/* <Input.Search
        placeholder="input search text"
        onSearch={(value) => {
          setSearchedText(value)
        }}
        enterButton
      /> */}
      <Input.Search
        style={{ marginBottom: "10px", textAlign: "center" }}
        placeholder="Search here ..."
        allowClear
        enterButton="Search"
        size="large"
        onSearch={(value) => {
          setSearchedText(value);
        }}
        onChange={(e) => {
          setSearchedText(e.target.value);
        }}
      />
      <Table
        className="antd-tablePost"
        scroll={{
          x: 1000,
          y: 240,
        }}
        columns={columns}
        rowKey={(record) => record.id}
        dataSource={data}
        pagination={{
          showSizeChanger: true,
          showQuickJumper: true,
          position: ["bottomCenter"],
          total: totalItems,
          showTotal: (total) => `Total ${total} items`,
        }}
        pageSize={tableParams.pagination.pageSize}
        loading={loading}
        onChange={handleTableChange}
        showTotal={(total) => `Total ${total} items`}
        footer={() => (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              border: "solid red 1px",
            }}
          >
            {/* <Pagination
              {...tableParams.pagination}
              total={tableParams.pagination.total}
              pageSize={tableParams.pagination.pageSize}
              onChange={handleTableChange}
              onShowSizeChange={handlePageSizeChange}
              showSizeChanger
              showQuickJumper
              showTotal={(total) => `Total ${total} items`}
            /> */}
          </div>
        )}
      />
    </div>
  );
};

export default TestTABLE;
