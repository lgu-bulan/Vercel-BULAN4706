import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Input, Modal, Table, Tag } from "antd";
import axios from "axios";
import { useEffect, useState } from "react";
import Swal from "sweetalert2";
import "./Table.scss";

function TablePosts() {
  const [data, setData] = useState();
  const [totalItems, setTotalItems] = useState();
  const [searchedText, setSearchedText] = useState("");
  const [loading, setLoading] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [editingStudent, setEditingStudent] = useState(null);
  const [tableParams, setTableParams] = useState({
    pagination: {
      current: 1,
      pageSize: 10,
      position: ["bottomCenter"],
      showQuickJumper: true,
      showSizeChanger: true,
    },
  });

  const columns = [
    {
      title: "Status",
      key: "status",
      dataIndex: "status",
      width: "100px",
      align: "center",
      fixed: "left",

      render: (_, { status }) => {
        // let color = status.length > 5 ? 'geekblue' : 'green';
        let color = "";
        if (status === "Draft") {
          color = "volcano";
        }
        if (status === "Published") {
          color = "geekblue";
        }
        return (
          <Tag color={color} key={status}>
            {status.toUpperCase()}
          </Tag>
        );
      },
    },
    {
      title: "PostID",
      dataIndex: "id",
      width: "80px",
      align: "center",
    },
    {
      title: "User",
      dataIndex: "uid",
      width: "80px",
      align: "center",
    },
    {
      title: "Title",
      dataIndex: "title",
      align: "center",
      filteredValue: [searchedText],
      onFilter: (value, record) => {
        return (
          String(record.title).toLowerCase().includes(value.toLowerCase()) ||
          String(record.uid).toLowerCase().includes(value.toLowerCase()) ||
          String(record.id).toLowerCase().includes(value.toLowerCase()) ||
          String(record.category).toLowerCase().includes(value.toLowerCase())
        );
        // return String(record.title.toLowerCase().includes(value.toLowerCase()));
      },

      width: "130px",
    },
    {
      title: "Description",
      dataIndex: "description",
      width: "160px",
      ellipsis: true,
    },
    {
      title: "Date",
      dataIndex: "date",
      width: "110px",
    },
    {
      title: "Category",
      key: "category",
      dataIndex: "category",
      width: "160px",
      align: "center", 

      render: (_, { category }) => {
        // let color = status.length > 5 ? 'geekblue' : 'green';
        let color = "";
        if (category === "LATEST NEWS") {
          color = "#5cdbd3";
        }
        if (category === "PESO UPDATES") {
          color = "#ff85c0";
        }
        if (category === "MUNICIPAL NEWS") {
          color = "#b37feb";
        }
        return (
          <Tag color={color} key={category}>
            {category.toUpperCase()}
          </Tag>
        );
      },
    },
    {
      title: "Action",
      dataIndex: "action here",
      width: "130px",
      fixed: "right",
      align: "center",
      render: (_,record) => {
        return (
          <>
            <EditOutlined
              onClick={() => {
                onEditStudent(record);
              }}
              style={{ color: "#4096ff", marginLeft: 15 }}
            />
            <DeleteOutlined
              onClick={() => {
                onDeletePost(record);
              }}
              style={{ color: "red", marginLeft: 12 }}
            />
          </>
        );
      },
    },
  ];

  //   ! FETCH DATA FROM API
  const fetchData = (page) => {
    setLoading(true);
    const { current, pageSize } = tableParams.pagination;
    axios
      .get(`http://localhost:5000/api/all?page=${current}&pageSize=${pageSize}&search=${searchedText}`)
      .then((res) => {
        const modifiedData = res.data.map((item) => ({
          ...item,
          key: item.id,
        }));
        console.log("res DAta", modifiedData.data);
        console.log("res DAta length", modifiedData.length);
        console.log("trying to get id ", modifiedData);
        console.log("total ", modifiedData.length);
        // console.log()
        setTotalItems(modifiedData.length);
        setData(modifiedData);
        setLoading(false);
        setTableParams((prevParams) => ({
          ...prevParams,
          pagination: {
            ...prevParams.pagination,
            total: res.data.totalCount,
          },
        }));
      })
      .catch((error) => {
        console.log(error);
        setLoading(false);
      });
  };
  //  ! Delete
  const onDeletePost = async (record) => {
    Modal.confirm({
      title: "Are you sure, you want to delete this posts record?",
      okText: "Yes",
      okType: "danger",
      onOk: async () => {
        try {
          await axios.delete(`http://localhost:5000/api/post/${record.id}`, {
            headers: {
              "Content-Type": "application/json",
              // Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
            },
          });
          // If the delete operation is successful, update the table data.
          const newData = data.filter((item) => item.id !== record.id);
          setData(newData);
          Swal.fire({
            title: "Success",
            icon: "success",
            text: "Product successfully deleted",
          });
        } catch (error) {
          console.log(error);
          Swal.fire({
            title: "Something Went Wrong",
            icon: "Error",
            text: "Please Try again",
          });
        }
      },
    });
  };
  
  //  ! Edit

  const onEditStudent = (record) => {
    setIsEditing(true);
    setEditingStudent({ ...record });
  };
  const resetEditing = () => {
    setIsEditing(false);
    setEditingStudent(null);
  };

  // !

  useEffect(() => {
    fetchData();
  },[]);

  useEffect(() => {
    fetchData(); // Fetch data again when the searched text changes
  }, [searchedText]);
  // !

  // ! HANDLERS

  const handleTableChange = (pagination, filters, sorter) => {
    setTableParams((prevParams) => ({
      ...prevParams,
      pagination,
      filters,
      ...sorter,
    }));

    if (pagination.pageSize !== tableParams.pagination?.pageSize) {
      setTableParams((prevParams) => ({
        ...prevParams,
        pagination: {
          ...prevParams.pagination,
          pageSize: pagination.pageSize,
          current: 1,
        },
      }));
    }

  };
  // !

  return (
    <div className="div-postTable">

      <Input.Search
        // style={{ marginBottom: "10px", textAlign: "center", width: 'min(65%, 1280px)' }}
        className="InputSearchTable"
        placeholder="Search here ..."
        allowClear
        enterButton="Search"
        size="large"
        onSearch={(value) => {
          setSearchedText(value);
        }}
        onChange={(e) => {
          setSearchedText(e.target.value);
        }}
      />
      <Table
        className="antd-tablePost"
        scroll={{
          x: 1000,
          y: 240,
        }}
        columns={columns}
        rowSelection={true}
        rowKey={(record) => record.id}
        dataSource={data}
        pagination={{
          ...tableParams.pagination,
          total: totalItems,
          showSizeChanger: true,
          showQuickJumper: true,
          position: ["bottomCenter"],
          onChange:{handleTableChange},
          showTotal: (total) => `Total ${total} items`,
        }}
        pageSize={tableParams.pagination.pageSize}
        loading={loading}
        onChange={handleTableChange}
        showTotal={(total) => `Total ${total} items`}
      />

      <Modal
        title="Edit Student"
        open={isEditing}
        okText="Save"
        onCancel={() => {
          resetEditing();
        }}
        onOk={() => {
          setData((pre) => {
            return pre.map((student) => {
              if (student.id === editingStudent.id) {
                return editingStudent;
              } else {
                return student;
              }
            });
          });
          resetEditing();
        }}
      >
        <Input
          value={editingStudent?.name}
          onChange={(e) => {
            setEditingStudent((pre) => {
              return { ...pre, name: e.target.value };
            });
          }}
        />
        <Input
          value={editingStudent?.email}
          onChange={(e) => {
            setEditingStudent((pre) => {
              return { ...pre, email: e.target.value };
            });
          }}
        />
        <Input
          value={editingStudent?.address}
          onChange={(e) => {
            setEditingStudent((pre) => {
              return { ...pre, address: e.target.value };
            });
          }}
        />
      </Modal>


    </div>
  );
}

export default TablePosts;
