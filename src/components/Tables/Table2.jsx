import React, { useState, useEffect } from "react";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Table, Input, Tag, Modal, Pagination } from "antd";
import axios from "axios";
import Swal from "sweetalert2";

function Table2() {
  const [searchedText, setSearchedText] = useState("");
  const [loading, setLoading] = useState(false);
  const [totalItems, setTotalItems] = useState(0);
  const [tableDataSource, setTableDataSource] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [selectedRows, setSelectedRows] = useState([]);
  const [disableActions, setDisableActions] = useState(true);

  const columns = [
    {
      title: "Status",
      key: "status",
      dataIndex: "status",
      width: "100px",
      align: "center",
      fixed: "left",

      render: (_, { status }) => {
        // let color = status.length > 5 ? 'geekblue' : 'green';
        let color = "";
        if (status === "Draft") {
          color = "volcano";
        }
        if (status === "Published") {
          color = "geekblue";
        }
        return (
          <Tag color={color} key={status}>
            {status.toUpperCase()}
          </Tag>
        );
      },
    },
    {
      title: "PostID",
      dataIndex: "id",
      width: "80px",
      align: "center",
    },
    {
      title: "User",
      dataIndex: "uid",
      width: "80px",
      align: "center",
    },
    {
      title: "Title",
      dataIndex: "title",
      align: "center",
      filteredValue: [searchedText],
      onFilter: (value, record) => {
        return (
          String(record.title).toLowerCase().includes(value.toLowerCase()) ||
          String(record.uid).toLowerCase().includes(value.toLowerCase()) ||
          String(record.id).toLowerCase().includes(value.toLowerCase()) ||
          String(record.category).toLowerCase().includes(value.toLowerCase())
        );
        // return String(record.title.toLowerCase().includes(value.toLowerCase()));
      },

      width: "130px",
    },
    {
      title: "Description",
      dataIndex: "description",
      width: "160px",
      ellipsis: true,
    },
    {
      title: "Date",
      dataIndex: "date",
      width: "110px",
    },
    {
      title: "Category",
      key: "category",
      dataIndex: "category",
      width: "160px",
      align: "center",

      render: (_, { category }) => {
        // let color = status.length > 5 ? 'geekblue' : 'green';
        let color = "";
        if (category === "LATEST NEWS") {
          color = "#5cdbd3";
        }
        if (category === "PESO UPDATES") {
          color = "#ff85c0";
        }
        if (category === "MUNICIPAL NEWS") {
          color = "#b37feb";
        }
        return (
          <Tag color={color} key={category}>
            {category.toUpperCase()}
          </Tag>
        );
      },
    },
    {
      title: "Action",
      dataIndex: "action here",
      width: "130px",
      fixed: "right",
      align: "center",
      render: (_, record) => {
        return (
          <>
            {selectedRows.length === 0 && (
              <>
                <EditOutlined
                onClick={() => {
                  // onEditStudent(record);
                }}
                style={{ color: "#4096ff", marginLeft: 15 }}
              />
              <DeleteOutlined
                onClick={() => {
                  handleDeletePost(record);
                }}
                style={{ color: "red", marginLeft: 12 }}
              />
              </>
            )}
            {/* <EditOutlined
              onClick={() => {
                // onEditStudent(record);
              }}
              style={{ color: "#4096ff", marginLeft: 15 }}
            />
            <DeleteOutlined
              onClick={() => {
                handleDeletePost(record);
              }}
              style={{ color: "red", marginLeft: 12 }}
            /> */}
          </>
        );
      },
    },
  ];
  const fetchRecords = (page) => {
    setLoading(true);
    axios
      .get(`http://localhost:5000/api/all?page=${page}&size=10`)
      .then((res) => {
        // console.log(res);
        const modifiedData = res.data.map((item) => ({
          ...item,
          key: item.id,
        }));
        setTotalItems(res.data.totalItems);
        setTableDataSource(modifiedData);
        setLoading(false);
      });
  };

  useEffect(() => {
    fetchRecords(currentPage);
  }, [currentPage]);

  useEffect(() => {
    console.log('Selected ROws: ',selectedRows)
    setDisableActions(selectedRows.length > 0);
  }, [selectedRows]);


  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  // [Section] Delete
  const handleDeletePost = async (record) => {
    // Your delete post logic here
    Modal.confirm({
      title: "Are you sure, you want to delete this posts record?",
      okText: "Yes",
      okType: "danger",
      onOk: async () => {
        try {
          await axios.delete(`http://localhost:5000/api/post/${record.id}`, {
            headers: {
              "Content-Type": "application/json",
              // Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
            },
          });
          // If the delete operation is successful, update the table data.
          const newData = tableDataSource.filter(
            (item) => item.id !== record.id
          );
          setTableDataSource(newData);
          Swal.fire({
            title: "Success",
            icon: "success",
            text: "Product successfully deleted",
          });
        } catch (error) {
          console.log(error);
          Swal.fire({
            title: "Something Went Wrong",
            icon: "Error",
            text: "Please Try again",
          });
        }
      },
    });
  };

  const handleCheckboxChange = (selectedRowKeys, selectedRows) => {
    setSelectedRows(selectedRowKeys);
  };

  const handleDeletePosts = async () => {
    if (selectedRows.length === 0) {
      // No records selected, display an error message or handle it accordingly
      return;
    }
  
    const selectedRecords = tableDataSource.filter(
      (record) => selectedRows.includes(record.key)
    );
  
    Modal.confirm({
      title: `Are you sure you want to delete ${selectedRecords.length} post(s)?`,
      okText: "Yes",
      okType: "danger",
      onOk: async () => {
        try {
          const deletePromises = selectedRecords.map(async (record) => {
            await axios.delete(`http://localhost:5000/api/post/${record.id}`, {
              headers: {
                "Content-Type": "application/json",
                // Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
              },
            });
          });
  
          // Wait for all delete operations to complete
          await Promise.all(deletePromises);
  
          // If the delete operations are successful, update the table data.
          const newData = tableDataSource.filter(
            (item) => !selectedRows.includes(item.key)
          );
          setTableDataSource(newData);
          
          // Clear the selected rows
          setSelectedRows([]);
  
          Swal.fire({
            title: "Success",
            icon: "success",
            text: "Post(s) successfully deleted",
          });
        } catch (error) {
          console.log(error);
          Swal.fire({
            title: "Something Went Wrong",
            icon: "error",
            text: "Please try again",
          });
        }
      },
    });
  };
    // [Section] Delete
  return (
    <div className="Table-Admin">
      <Input.Search onChange={(e) => setSearchedText(e.target.value)} />

      <Table
        scroll={{
          x: 1000,
          y: 240,
        }}
        bordered
        rowSelection={{
          onChange: handleCheckboxChange,
          selectedRowKeys: selectedRows,
        }}
        rowKey={(record) => record.id}
        loading={loading}
        columns={columns}
        dataSource={tableDataSource}
        // pagination={false}
        pagination={{
          // ...tableParams.pagination,
          // total: totalItems,
          showSizeChanger: true,
          showQuickJumper: true,
          position: ["bottomCenter"],
          // onChange:{handleTableChange},
          // showTotal: (total) => `Total ${total} items`,
        }}
      />

      {selectedRows.length > 0 && (
        <button onClick={handleDeletePosts}>
          Delete Selected
        </button>
      )}
    </div>
  );
}

export default Table2;
