import React from 'react';
import { Card, Col, Container, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
// import PropTypes from 'prop-types';
// import { Link } from 'react-router-dom';
import "./newsPublication.css";

function newsCard() {

  return (
    
    <Container id="newsPubContainer">
        <Row id="cardRow" xs={1} md={4}  className="g-4">
            {Array.from({ length: 4 }).map((_, idx) => (
                <Col>
                    <Card>
                        <Card.Img  
                        variant="top" 
                        alt="Card image" 
                        src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZNN_ea_96nLpLRDRRkkb2_kZOYTWA-joXfA&usqp=CAU"
                        width= "239px"
                        height= "169px" />
                        <Card.Body>
                            <Card.Title>Card title</Card.Title>
                            <Card.Text>
                                This is a longer card with supporting text below as a natural
                                lead-in to additional content. This content is a little bit
                                longer.
                            </Card.Text>
                            <Link id="tag-links" className="btn btn-success" src="#">test Tag</Link>
                        </Card.Body>
                    </Card>
                </Col>
            ))}
        </Row>
    </Container>
    

)
}



export default newsCard