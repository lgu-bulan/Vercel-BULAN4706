import React from "react";
import { Tabs } from "antd";
import './TabsComponent.css'
// const { TabPane } = Tabs;
function TabsComponent({ defaultActiveKey, type, size, tabItems}) {
  return (
    <div>
      {/* <Tabs defaultActiveKey={defaultActiveKey} type={type} size={size}>
        {tabItems.map((tab, i) => (
          <Tabs.TabPane key={i + 1} tab={tab.label}>
            {tab.children}
          </Tabs.TabPane>
        ))}
      </Tabs> */}
      <Tabs
        className="Tabs-Component"
        defaultActiveKey={defaultActiveKey}
        type={type}
        size={size}
        items={tabItems.map((tab, i) => ({
          ...tab,
          key: String(i + 1),
        }))}
      />
        
    </div>
  );
}

export default TabsComponent;
