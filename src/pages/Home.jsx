import React from "react";
import ArticleCard from "../components/ArticleCard/articlePosts";
import Footer from "../components/Footer/Footer";
import PreFooter from "../components/Footer/pre-footer";
import GovServices from "../components/GovServices/GovServices";
import GotoTop from "../components/GtoTop/ButtonTop";
import Jumbotron from "../components/Jumbotron/Jumbotron";
import Minfo from "../components/MInfo/MayorInfo";
import Counter from "../components/MunicipalOverview/CounterMO";
import MunicipalOverview from "../components/MunicipalOverview/MunicipalOveriew";

// TEST
function Home() {
  return (
    <>
      {/* test */}
      {/* <HistoryTown /> */}
      {/*  */}
      <Jumbotron />
      <Minfo />
      <MunicipalOverview />
      <Counter />
      {/* <UploadFile/> */}
      {/* <NewsAndPub /> */}
      <ArticleCard />
      <GovServices />
      {/* <SingleArticleView/> */}
      {/* <WritePostTest /> */}
      {/* <ContentPage /> */}
      <GotoTop />
      <PreFooter />
      <Footer />
    </>
  );
}

export default Home;


// import React from "react";
// import Jumbotron from "../components/Jumbotron/Jumbotron";
// import Minfo from "../components/MInfo/MayorInfo";
// import NewsAndPub from "../components/NewsAndPub/newsCard";
// import GovServices from "../components/GovServices/GovServices";
// import GotoTop from "../components/GtoTop/ButtonTop";
// import PreFooter from "../components/Footer/pre-footer";
// import Footer from "../components/Footer/footer";
// import MunicipalOverview from "../components/MunicipalOverview/MunicipalOveriew";
// import Counter from "../components/MunicipalOverview/CounterMO";
// import UploadFile from "../components/UploadFiles/UploadFiles";
// import ArticleCard from "../components/ArticleCard/articlePosts";
// import SingleArticleView from "../components/SingleArticleView/SingleArticleView";

// import WritePostTest from "../components/WritePost/WritePostTest";
// import ContentPage from "./ContentPage/ContentPage";
// import { Container } from "react-bootstrap";
// // TEST
// import HistoryTown from "./SubPages/History-Town/HistoryTown";
// function Home() {
//   return (
//     <>
//       {/* test */}
//       {/* <HistoryTown /> */}
//       {/*  */}
//       <Jumbotron />
//       <Minfo />
//       <MunicipalOverview />
//       <Counter />
//       {/* <UploadFile/> */}
//       {/* <NewsAndPub /> */}
//       <ArticleCard />
//       <GovServices />
//       {/* <SingleArticleView/> */}
//       {/* <WritePostTest /> */}
//       {/* <ContentPage /> */}
//       <GotoTop />
//       <PreFooter />
//       <Footer />
//     </>
//   );
// }

// export default Home;

