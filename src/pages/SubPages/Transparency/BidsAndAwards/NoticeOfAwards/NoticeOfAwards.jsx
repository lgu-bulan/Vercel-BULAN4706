import React, { useEffect, useState } from "react";
import axios from "axios";
import "../../cs/Downloadables.scss";
import {
  FilePdfTwoTone,
  FileWordTwoTone,
  CalendarOutlined,
  HddOutlined,
  DownloadOutlined,
  FileImageTwoTone,
} from "@ant-design/icons";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { Pagination, Col, Row, Select, Divider, Input } from "antd";
const { Search } = Input;

function NoticeOfNegotiatedProcurement() {
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const [dataDownload, SetDataDownload] = useState([]);
  const perPage = 10;
  const [currentPage, setCurrentPage] = useState(1);
  // const [totalItems, setTotalItems] = useState(0);
  const history = useNavigate();
  let option = [
    {
      label: "OrderBy",
      options: [
        {
          label: "Date",
          value: "date",
        },
        {
          label: "Larger File Size",
          value: "LargeFileSize",
        },
        {
          label: "Smaller File Size",
          value: "SmallFileSize",
        },
      ],
    },
  ];

  const updateURLParams = (params) => {
    const searchParams = new URLSearchParams(params);
    history.push(`?${searchParams.toString()}`);
  };

  const [searchQuery, setSearchQuery] = useState(
    queryParams.get("search") || ""
  );

  const filteredData = dataDownload.filter((data) =>
    data.fileName.toLowerCase().includes(searchQuery.toLowerCase())
  );

  const handleSearchChange = (e) => {
    const newSearchQuery = e.target.value;
    setSearchQuery(newSearchQuery);
    setCurrentPage(1);
    updateURLParams({ sort: sortBy, search: newSearchQuery });
  };

  // const handleInputChange = (event) => {
  //   setSearchQuery(event.target.value);
  // };

  const [sortBy, setSortBy] = useState(queryParams.get("sort") || "date");

  const convertToBytes = (fileSize) => {
    const size = parseFloat(fileSize);

    if (fileSize.includes("MB")) {
      return size * 1024 * 1024;
    } else if (fileSize.includes("KB")) {
      return size * 1024;
    } else if (fileSize.includes("GB")) {
      return size * 1024 * 1024 * 1024;
    } else if (fileSize.includes("B")) {
      return size;
    }

    return 0;
  };

  const sortedData = [...filteredData].sort((a, b) => {
    const fileSizeA = convertToBytes(a.file_size);
    const fileSizeB = convertToBytes(b.file_size);

    if (sortBy === "date") {
      return new Date(b.date) - new Date(a.date);
    } else if (sortBy === "LargeFileSize") {
      return fileSizeB - fileSizeA;
    } else if (sortBy === "SmallFileSize") {
      return fileSizeA - fileSizeB;
    }
    return 0;
  });

  const handleSortChange = (value) => {
    setSortBy(value);
    setCurrentPage(1);
    updateURLParams({ sort: value, search: searchQuery });
  };

  const fetchData = async () => {
    try {
      const response = await axios.get("http://localhost:5000/api/Allfiles");
      SetDataDownload(response.data);
      // setTotalItems(response.data.length);
      console.log(response.data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchData(sortBy, searchQuery);
  }, [sortBy, searchQuery]);

  return (
    <div className="card-downloadbale-body">
      <div className="card-downloadbale-banner">
        <div className="downloadable-page-title">
          <h1>Request For Quotation</h1>
          <p className="span-bar"></p>
        </div>
        <div className="custom-shape-divider-bottom-1685689813">
          <svg
            data-name="Layer 1"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 1200 120"
            preserveAspectRatio="none"
          >
            <path
              d="M985.66,92.83C906.67,72,823.78,31,743.84,14.19c-82.26-17.34-168.06-16.33-250.45.39-57.84,11.73-114,31.07-172,41.86A600.21,600.21,0,0,1,0,27.35V120H1200V95.8C1132.19,118.92,1055.71,111.31,985.66,92.83Z"
              className="shape-fill"
            ></path>
          </svg>
        </div>
      </div>

      <div className="Filter-Menu">
        <Search
          placeholder="Search..."
          enterButton
          className="downloadables_searchbar"
          value={searchQuery}
          onChange={handleSearchChange}
        />

        <Select
          className="downloadables_sort1"
          defaultValue="date"
          onChange={handleSortChange}
          style={{
            width: 200,
          }}
          options={option}
        />
      </div>
      {sortedData.length === 0 ? (
        <div className="card-downloadbale-container-noresult">
          <h1>No files available.</h1>
        </div>
      ) : (
        <div className="card-downloadbale-container">
          {sortedData
            ?.slice((currentPage - 1) * perPage, currentPage * perPage)
            .map((data) => (
              <Row
                xs={1}
                md={3}
                lg={3}
                className="download-Cards d-flex align-items-center"
                key={data.id}
              >
                <Col xs={24} md={2} lg={2}>
                  <div className="mimetype-icons-div d-flex align-items-center justify-content-center p-2 m-2">
                    {data.mime_type === "application/pdf" ? (
                      <FilePdfTwoTone
                        className="icon-downloadables"
                        twoToneColor="#f5222d"
                      />
                    ) : data.mime_type === "application/msword" ? (
                      <FileWordTwoTone
                        className="icon-downloadables"
                        twoToneColor="#1677ff"
                      />
                    ) : data.mime_type === "image/jpeg" || "image/png" ? (
                      <FileImageTwoTone className="icon-downloadables" />
                    ) : (
                      <></>
                    )}
                  </div>
                </Col>

                <Col xs={24} md={18} lg={18}>
                  <div className="card_downloable_title d-flex align-items-center">
                    <h5 style={{ margin: "0" }}>{data.fileName}</h5>
                  </div>
                  <Divider className="mx-2 my-0" style={{ width: "600px" }} />
                  <div className="card_downloable_info d-flex align-items-center">
                    <CalendarOutlined
                      style={{ marginRight: "8px", fontSize: "18px" }}
                    />{" "}
                    <p className="pe-3">
                      Date Uploaded:{" "}
                      {`${data.date.slice(0, 10)} ${data.date.slice(10, 19)}`}
                    </p>
                    <HddOutlined
                      style={{ marginRight: "8px", fontSize: "18px" }}
                    />{" "}
                    <p className="pe-3">FileSize: {data.file_size}</p>
                  </div>
                </Col>

                <Col xs={24} md={4} lg={4}>
                  <div className="card_downloadble_action">
                    <Link
                      to={data.file_url}
                      target="_blank"
                      style={{ textDecoration: "none" }}
                    >
                      <button
                        style={{
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        Download
                        <DownloadOutlined
                          style={{
                            marginLeft: "12px",
                            fontSize: "1.2rem",
                            fontWeight: "bolder",
                          }}
                        />
                      </button>
                    </Link>
                  </div>
                </Col>
              </Row>
            ))}
        </div>
      )}

      {/* Pagination */}
      <div className="div-Pagination">
        <Pagination
          current={currentPage}
          pageSize={perPage}
          total={sortedData.length}
          onSearch={handleSearchChange}
          onChange={setCurrentPage}
          showTotal={(total, range) =>
            `${range[0]}-${range[1]} of ${total} items`
          }
        />
      </div>
    </div>
  );
}

export default NoticeOfNegotiatedProcurement;
