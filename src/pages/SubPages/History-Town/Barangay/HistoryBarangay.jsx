import React from "react";
import TabsComponent from "../../../../components/Tab/TabsComponent";
import BarangayTimeLine from "./BarangayTimeLine";
import DevelopmentGrowth from "./DevelopmentGrowth";
import "./HistoryBarangay.scss";
function HistoryBarangay() {
  const tabItems = [
    {
      label: "Barangay TimeLine",
      children: <BarangayTimeLine />,
    },
    {
      label: "Development and Growth",
      children: <DevelopmentGrowth />,
    },
  ];
  return (
    <div className="History-Barangay-Container">
      <TabsComponent
        defaultActiveKey="1"
        type="card"
        size="large"
        tabItems={tabItems}
        
      />
      {/* <div>HistoryBarangay</div> */}
    </div>
  );
}

export default HistoryBarangay;
