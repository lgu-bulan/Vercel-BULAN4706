import React from "react";

function DevelopmentGrowth() {
  return (
    <div className="Development-Growth-Container">
      <div className="Development-Growth-pr1">
        <h3>THE ANCIENT BARANGAYS OF BULAN</h3>
        <p>
          Long before the Spaniards came to the Philippines, and to Bulan, our
          place must already have been dotted with settlements and communities,
          from the edge of what is now Sitio Inalapan (Danao) to the North and
          “Botag” to the south. Even the mainland of Bulan must have these
          villages already. The proofs are the archeological artifacts of jar
          burial sites such as those in Fabrica, San Juan Daan and Taromata and
          those communities doing trade and commerce with Chinese traders as can
          be evidenced by the Chinese wares excavated in places like Liman,
          Magsaysay, Gate, and other areas in Bulan.
        </p>
        <p>
          These settlements were called Barangays,composed of more orless
          100inhabitants. The leaders or heads of these settlements were called
          Datu, or Panginoo or Pangolo. The word barangay originally meant the
          boat used by the migrant Malays who settled in our country around 200
          B.C. When the Spanish colonizers came, they retained that name for the
          settlements and called the village headman as Cabeza de Barangay.
        </p>
      </div>

      <div className="Development-Growth-pr2">
        <h3>BARANGAYS UNDER THE SPANIARDS</h3>
        <p>
          In July 1569, the Spanish colonizers sailing from Masbate dropped
          anchor in Otavi and here they celebrated the very firstholy Mass in
          the island of Luzon. A settlement up north was called Bililan. In
          1640, Gate was made a visita of Bulusan. In 1690, it became a separate
          Pueblo Civil and Parish. During these times, whatever settlement there
          might have been recognized was already headed by a Cabeza de Barangay.
          In 1746, Gate, together with many other settlements in Bulan and
          Sorsogon Province was raided, pillaged, and burned by the Moros. The
          survivors of the raid sought refuge in the mountains. Some settled in
          what is now San Juan Daan. Due to the incessant Moro raids, many
          sought refuge in the hinterlands; the coastal areas were deserted.
        </p>
      </div>

      <div className="Development-Growth-pr3">
        <h3>BULAN REBUILT: THE FIRST OFFICIAL BARANGAYS OF BULAN</h3>
        <p>
          In 1801, when Bulan was refounded along the Maribok River, four
          barangays were officially recognized as cabeceras or Districts of
          Bulan: Otavi, Gate, San Juan (Daan) and Buenavista (in Irosin). Each
          was headed by a Cabeza. These Cabezas were appointed by the Parish
          Priest. In the 1860s upto the 1900, the Cabezas were appointed by the
          Capitanes del Pueblo or the equivalent of the Town Mayor now. The very
          first officially recognized Cabezas of Bulan in 1801 were Don Juan
          Austria of Otavi, Don Juan Cipriano of Gate, Don Juan Selleso of San
          Juan and Don Juan Santiago of Buenavista.
        </p>
      </div>

      <div className="Development-Growth-pr4">
        <h3>THE MAURA LAW</h3>
        <p>
          In 1893, the Maura Law was passed. This Spanish Law established
          tribunales, municipals/ pueblos and juntas provinciales. The pueblos
          were divided and subdivided into barrios (wards) and barangays under
          tenientes del barrio and cabezas de barangay, respectively. Barangays
          were villages with 100-150 families. Some barangays of Bulan were
          established during this period.
        </p>
      </div>

      <div className="Development-Growth-pr5">
        <h3>BARRIOS UNDER THE AMERICANS</h3>
        <p>
          In 1899, the American Government issued General Orders Nos. 40 and 43.
          The barangays were renamed as Barrios. The Barrio head was still the
          Cabeza, who at the same time was a member of the Municipal Council. By
          this time, Bulan had 11 Barrios, aside from the Poblacion: Inalapan
          (now, Danao), Namo, Inararan, Calomagon, San Francisco, Bignin, San
          Juan Bag-o, Capacuhan (San Ramon), Gate, Otabi, Botag. In 1901,
          Jamor-awon became a Barrio. In 1901, under Act No. 82 or the Municipal
          Code, the Cabeza became a Barrio Lieutenant or “Teniente del Barrio”.
          He was appointed by a Municipal Councilor. In 1903, Bulan had 12
          Barrios plus the Poblacion. In 1918, J. Gerona, Marinab, and Padre
          Diaz were already included officially in the census. There were now
          already 15 barrios. In 1939, Barrio Quezon, Recto, and R. Gerona were
          already in the census list. In 1931, Act No. 3861 was adopted, and a
          barrio council was organized: the Lieutenant and four councilors. They
          were all appointed by a Municipal Councilor. Barrios were also created
          by acts of Congress (until 1963).
        </p>
      </div>

      <div className="Development-Growth-pr6">
        <h3>BARRIO CHARTERS</h3>
        <p>
          By virtue of R.A. 1245 of 1955, the first Barrio election was held on
          January 19, 1956. On June 20, 1959, RA 2370 or the Barrio Charter Act
          was passed. In 1963, the revised Barrio Charter (RA 3590, amending RA
          2370, Barrio Charter) was enacted.The title Barrio Lieutenantwas
          changed to Barrio Captain. There were 21 barrios in Bulan created
          under the Revised Barrio Charter. On January 9, 1969, during the
          incumbency of Mayor Luis G. de Castro, Sr., Somagongsong, Dolos,
          Montecalvario, Sigad, Sta. Teresita were constituted. And in 1972,
          under Provincial Board Resolutions No. 112, Obrero, Managanaga,
          Libertad, Rizal, Imelda, Osmena, Bonifacio, Calpi, Quirino, Taromata,
          Roxas, Sagrada, and Abad Santos were created.
        </p>
      </div>

      <div className="Development-Growth-pr6">
        <h3>“BARANGAYS” ONCE MORE</h3>
        <p>
          In 1973, when the 1973 Constitution was ratified, barrios in cities
          and town centers (Poblacion) were created. The Poblacion was divided
          into eight zones. On September 21, 1974, PD No. 557 was promulgated
          declaring all Barrios in the Philippines as Barangays.By 1975, there
          were already 63 barangays in Bulan, including the eight zones created
          in 1973. In 1983, the Local Government Code (BP 337) was enacted, and
          the governance of the barangay units was also stipulated in this law.
          In 1991, RA 7160 was approved, effectively giving autonomy and more
          powers to the barangay Governments.
        </p>
      </div>
    </div>
  );
}

export default DevelopmentGrowth;
