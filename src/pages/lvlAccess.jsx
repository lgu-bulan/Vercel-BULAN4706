import React, {useState, useEffect} from 'react'
import tableData from '../components/tableData'

function lvlaccess() {

  const[AllaccessData, setAllAccessData] = useState([]) 
  const[error, setError] = useState()

  useEffect(() =>{

    async function fetchData {
      try {
        const response = await fetch('http://localhost:5000/api/lvl1Access');
        const json = await response.json();
        setAllAccessData(json);
      } catch (err) {
        setError(err);
      }

    }
  },[]);
  
   
   
  
  
  

  

  return (
    <>
        {error ? <div>{error.message}</div> : null}
        <tableData lvlAccessData = {AllaccessData} fetchData = {fetchData}/>
    </>
  )
}

export default lvlaccess