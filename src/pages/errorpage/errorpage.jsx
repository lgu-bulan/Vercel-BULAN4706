import React, { useEffect } from "react";

import TestAppNavbar from "../../components/navbar/NavBarTest";
import "./errorpage.css";

function Errorpage({ isAuthenticated }) {
  useEffect(() => {
    document.body.style.backgroundColor = "#416475";
    document.body.style.marginBottom = "50px";
    return () => {
      document.body.style.backgroundColor = null;
      document.body.style.marginBottom = null;
    };
  }, []);

  return (
    <>
      {!isAuthenticated ? <></> : <TestAppNavbar />}
      <div className="div-error-page-container">
        <section className="error-container">
          <span>4</span>
          <span>
            <span className="screen-reader-text">0</span>
          </span>
          <span>4</span>
        </section>
        <h1 className="error-page-title-h1"> Error Page</h1>
        <p className="zoom-area">
          <b>Ooops..How did you end up here.</b>
        </p>
        <div className="link-container">
          <a className="more-link" href="/">
            Return to Homepage
          </a>
        </div>
      </div>
    </>
  );
}

export default Errorpage;
