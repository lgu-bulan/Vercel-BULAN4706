import React from "react";
import "./ContentPage.scss";

import TestPostTable from "../../components/Tables/TablePosts";

function ContentPage() {
  return (
    <div className="div-ContentPage">
      {/* <PostTable /> */}
      <TestPostTable className='content-postTable' />
    </div>
  );
}

export default ContentPage;
