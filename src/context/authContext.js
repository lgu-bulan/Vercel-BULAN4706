import axios from "axios";
import { createContext, useEffect, useState } from "react";
export const AuthContext = createContext()

export const AuthContexProvider = ({ children }) => {
    const [currentUser, setCurrentUser] = useState(JSON.parse(localStorage.getItem("user") || null))
    const [isAuthenticated, setIsAuthenticated] = useState(JSON.parse(localStorage.getItem("AuthenticationStatus") || false));


    const login = async (inputs) => {
        try {
            const response = await axios.post("http://localhost:5000/api/auth/LogIn", inputs);
            setCurrentUser(response.data);
            setIsAuthenticated(true);
            console.log(response);
            console.log(response.data);
            console.log('successful');
            console.log(isAuthenticated)
            return response;
        } catch (error) {
            console.error(error);
            throw new Error('Login failed');
        }
    };


    const unsetUser = () => {
        localStorage.clear()
        setCurrentUser(null)
    }



    // const logout = async (inputs) => {
    //     const res = await axios.post("/auth/logout");
    //     setCurrentUser(null);
    // }


    useEffect(() => {
        localStorage.setItem("user", JSON.stringify(currentUser));
        localStorage.setItem("AuthenticationStatus", JSON.stringify(isAuthenticated));

    }, [currentUser, isAuthenticated]);

    return (
        <AuthContext.Provider value={{ currentUser, isAuthenticated, setIsAuthenticated, login, unsetUser }}>
            {children}
        </AuthContext.Provider>
    )

}   